import * as React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { ApplicationState } from "store";
import { startVerification, resetVerification } from "@store/emailVerification/actions";
import {
    EmailVerificationState,
    EmailVerificationActionTypes,
} from "@store/emailVerification/types";

export interface ContainerOwnProps {
    children: (...args: any[]) => React.ReactNode;
}

export interface ContainerStoreProps extends EmailVerificationState {}

export interface ContainerDispatchProps {
    startVerification: (email: string) => void;
    resetVerification: () => void;
}

export type ContainerProps = ContainerStoreProps & ContainerDispatchProps & ContainerOwnProps;

const mapStateToProps = (state: ApplicationState): ContainerStoreProps => state.emailVerification;
const mapDispatchToProps = (dispatch): ContainerDispatchProps => ({
    startVerification: (email: string) => {
        dispatch(startVerification(email));
    },
    resetVerification: () => {
        dispatch(resetVerification());
    },
});

class EmailVerificationContainer extends React.Component<ContainerProps, undefined> {
    static defaultProps = {
        startVerification: email => {},
        resetVerification: () => {},
    };

    render() {
        const { children, ...restProps } = this.props;

        return children(restProps);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerificationContainer);
