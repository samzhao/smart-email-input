import { createStore, applyMiddleware, compose, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { ApplicationState, reducers, sagas } from "@store/index";

declare let module: { hot: any };

export default function configureStore(initialState?: ApplicationState): Store<ApplicationState> {
    const sagaMiddleware = createSagaMiddleware();
    let middleware = [sagaMiddleware];
    let composeEnhancers = compose;

    // setup for development
    if (process.env.NODE_ENV === "development" && typeof window === "object") {
        // setup for Redux devtools browser extension
        if ((<any> window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
            composeEnhancers = (<any> window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
        }
    }

    const enhancer = composeEnhancers(applyMiddleware(...middleware));

    const store: Store<ApplicationState> = createStore(
        reducers,
        initialState,
        enhancer
    );

    sagaMiddleware.run(sagas);

    // webpack HMR
    if (process.env.NODE_ENV === "development" && module.hot) {
        module.hot.accept("../store", () => {
            const { reducers: nextReducers } = require("../store/index");
            store.replaceReducer(nextReducers);
        });
    }

    return store;
}
