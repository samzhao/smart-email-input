import { run } from "mailcheck";

export default function getEmailSuggestion(email: string): Promise<string> {
    return new Promise((resolve, reject) => {
        run({
            email,
            suggested: suggestion => {
                const { full } = suggestion;
                const suggestionText = full === email ? "" : full;

                resolve(suggestionText);
            },
            empty: () => {
                const emptyError = new Error(`Cannot determine email suggestion for input: ${email}`);

                reject(emptyError);
            },
        });
    });
}
