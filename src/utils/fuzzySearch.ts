import * as Fuse from "fuse.js";

const defaultOptions: Fuse.FuseOptions = {
    caseSensitive: false,
    shouldSort: true,
    includeScore: false,
    includeMatches: false,
    threshold: 0.5,
};

export default function fuzzySearch(input: string, source: any[], options?: Fuse.FuseOptions): any[] {
    const fuse = new Fuse(source, { ...defaultOptions, ...options });

    return fuse.search(input);
}
