import { combineReducers, Dispatch, Reducer } from "redux";
import { all, fork } from "redux-saga/effects";

import { EmailVerificationState } from "@store/emailVerification/types";
import emailVerificationReducer from "@store/emailVerification/reducer";
import { watchStartEmailVerification } from "@store/emailVerification/sagas";

export interface ApplicationState {
    emailVerification: EmailVerificationState;
}

export const reducers: Reducer<ApplicationState> = combineReducers<ApplicationState>({
    emailVerification: emailVerificationReducer,
});

export function* sagas() {
    yield all([
        fork(watchStartEmailVerification),
    ]);
}
