import { Reducer } from "redux";
import {
    EmailVerificationAction,
    EmailVerificationActionTypes,
    EmailVerificationState,
} from "./types";

export const initialState: EmailVerificationState = {
    isVerified: undefined,
    isVerifying: false,
    hasError: false,
    error: undefined,
    message: undefined,
};

const {
    EMAIL_VERIFICATION_IN_PROGRESS,
    EMAIL_VERIFICATION_SUCCEEDED,
    EMAIL_VERIFICATION_FAILED,
    RESET_EMAIL_VERIFICATION,
} = EmailVerificationActionTypes;

const reducer: Reducer<EmailVerificationState> =
    (state: EmailVerificationState = initialState, action: EmailVerificationAction) => {
        switch (action.type) {

            case EMAIL_VERIFICATION_IN_PROGRESS:
                const message = action.payload;

                return {
                    ...initialState,
                    isVerifying: true,
                    ...(message ? { message } : {}),
                };

            case EMAIL_VERIFICATION_SUCCEEDED:
                return {
                    ...initialState,
                    isVerified: true,
                };

            case EMAIL_VERIFICATION_FAILED:
                const { error } = action.payload;

                return {
                    ...initialState,
                    isVerified: false,
                    hasError: error == null ? false : true,
                    error,
                };

            case RESET_EMAIL_VERIFICATION:
                return initialState;

            default:
                return state;

        }
    };

export default reducer;
