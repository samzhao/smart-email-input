import { Action } from "redux";
import { EmailVerificationResult, EmailVerificationError } from "@resources/emailVerification/types";

// the STARTED action is fired before the API request is sent
// while the IN_PROGRESS action is fired while API request is pending
export enum EmailVerificationActionTypes {
    EMAIL_VERIFICATION_STARTED = "@@emailVerification/VERIFICATION_STARTED",
    EMAIL_VERIFICATION_IN_PROGRESS = "@@emailVerification/VERIFICATION_IN_PROGRESS",
    EMAIL_VERIFICATION_SUCCEEDED = "@@emailVerification/VERIFICATION_SUCCEEDED",
    EMAIL_VERIFICATION_FAILED = "@@emailVerification/VERIFICATION_FAILED",
    RESET_EMAIL_VERIFICATION = "@@emailVerification/RESET_VERIFICATION",
}

export interface EmailVerificationState {
    isVerified: boolean;
    isVerifying: boolean;
    hasError: boolean;
    error: EmailVerificationError;
    message: string; // arbitrary message for friendly status updates
}

export interface EmailVerificationStartedAction extends Action {
    type: EmailVerificationActionTypes.EMAIL_VERIFICATION_STARTED;
    payload: {
        email: string;
    };
}

export interface EmailVerificationInProgressAction extends Action {
    type: EmailVerificationActionTypes.EMAIL_VERIFICATION_IN_PROGRESS;
    payload?: string;
}

export interface EmailVerificationSucceededAction extends Action {
    type: EmailVerificationActionTypes.EMAIL_VERIFICATION_SUCCEEDED;
}

export interface EmailVerificationFailedAction extends Action {
    type: EmailVerificationActionTypes.EMAIL_VERIFICATION_FAILED;
    payload: {
        error: EmailVerificationError;
    };
}

export interface ResetEmailVerificationAction extends Action {
    type: EmailVerificationActionTypes.RESET_EMAIL_VERIFICATION;
}

export type EmailVerificationAction =
    | EmailVerificationStartedAction
    | EmailVerificationInProgressAction
    | EmailVerificationSucceededAction
    | EmailVerificationFailedAction
    | ResetEmailVerificationAction;
