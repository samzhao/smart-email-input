import { delay } from "redux-saga";
import { fork, call, put, cancel, all, take, takeEvery } from "redux-saga/effects";
import { verificationInProgress, verificationSucceeded, verificationFailed } from "./actions";
import { EmailVerificationActionTypes, EmailVerificationAction } from "@store/emailVerification/types";
import EmailVerificationAPI from "@resources/emailVerification/api";
import { EmailVerificationResult } from "@resources/emailVerification/types";
import validateEmail from "@utils/validateEmail";

const uniqueAction = (actionType: EmailVerificationActionTypes) => {
    let lastPayload = {
        email: undefined,
    };

    return ({ type, payload }) => {
        if (type !== actionType) return false;
        if (payload.email === lastPayload.email) {
            return false;
        }

        lastPayload = payload;
        return true;
    };
};

function* showProgressStatus() {
    yield put(verificationInProgress());

    // show a message if the request takes longer than usual
    yield call(delay, 500);
    yield put(verificationInProgress("Checking email address..."));
}

function* verifyEmailViaAPI(email) {
    try {
        const resp = yield call(EmailVerificationAPI.verifyEmail, email);
        const result = new EmailVerificationResult(resp);

        if (result.isValid()) {
            yield put(verificationSucceeded());
        } else if (result.isInvalid()) {
            yield put(verificationFailed({
                message: validateEmail(email)
                    ? "Sorry, we can't deliver to that email address. Try another one?"
                    : "Please make sure you have entered the email correctly.",
            }));
        } else if (result.hasError()) {
            yield put(verificationFailed({
                message: "Sorry, we can't seem to deliver to your email address :(",
            }));
        } else {}
    } catch (error) {
        yield put(verificationFailed());
    }
}

export function* startEmailVerification(action) {
    const { email } = action.payload;

    const apiCall = yield fork(verifyEmailViaAPI, email);
    const showInProgress = yield fork(showProgressStatus);

    yield take([
        EmailVerificationActionTypes.EMAIL_VERIFICATION_SUCCEEDED,
        EmailVerificationActionTypes.EMAIL_VERIFICATION_FAILED,
    ]);
    yield cancel(showInProgress);
}

export function* watchStartEmailVerification() {
    yield all([
        takeEvery(uniqueAction(EmailVerificationActionTypes.EMAIL_VERIFICATION_STARTED), startEmailVerification),
    ]);
}
