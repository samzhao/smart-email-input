import { ActionCreator } from "redux";
import {
    EmailVerificationAction,
    EmailVerificationActionTypes,
} from "./types";
import { EmailVerificationResult } from "@resources/emailVerification/types";

const {
    EMAIL_VERIFICATION_STARTED,
    EMAIL_VERIFICATION_IN_PROGRESS,
    EMAIL_VERIFICATION_SUCCEEDED,
    EMAIL_VERIFICATION_FAILED,
    RESET_EMAIL_VERIFICATION,
} = EmailVerificationActionTypes;

export const startVerification: ActionCreator<EmailVerificationAction> =
    (email: string) => ({
        type: EMAIL_VERIFICATION_STARTED,
        payload: { email },
    });

export const verificationInProgress: ActionCreator<EmailVerificationAction> =
    (message?: string) => ({
        type: EMAIL_VERIFICATION_IN_PROGRESS,
        payload: message,
    });

export const verificationSucceeded: ActionCreator<EmailVerificationAction> =
    () => ({
        type: EMAIL_VERIFICATION_SUCCEEDED,
    });

export const verificationFailed: ActionCreator<EmailVerificationAction> =
    (error?) => ({
        type: EMAIL_VERIFICATION_FAILED,
        payload: { error },
    });

export const resetVerification: ActionCreator<EmailVerificationAction> =
    () => ({
        type: RESET_EMAIL_VERIFICATION,
    });
