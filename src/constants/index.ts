import { defaultDomains } from "mailcheck";

defaultDomains.push("yahoo.com", "yahoo.ca", "yahoo.co.uk");

export const EMAIL_DOMAINS = defaultDomains;
