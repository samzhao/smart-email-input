import * as React from "react";
import "./App.scss";

import EmailVerificationContainer, { ContainerProps as EmailVerificationContainerProps } from "@containers/EmailVerification";
import EmailInput from "@organisms/EmailInput";
import { EmailVerificationState } from "@store/emailVerification/types";
import { Button } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

export interface AppProps {}

export default class App extends React.Component<AppProps> {
    onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
    }

    renderEmailInput = (storeProps: EmailVerificationContainerProps) => {
        return (
            <div>
                <div style={{marginBottom: 30}}>
                    <EmailInput {...storeProps} />
                </div>
                <div>
                    <Button
                        className="btn-primary"
                        disabled={!storeProps.isVerified}
                        icon={IconNames.IMPORT}
                        type="submit"
                        fill
                        large
                    >
                        Create FREE Invoice Now!
                    </Button>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="app">
                <div className="container">
                    <form className="form" onSubmit={this.onSubmit}>
                        <EmailVerificationContainer>
                            {this.renderEmailInput}
                        </EmailVerificationContainer>
                    </form>
                </div>
            </div>
        );
    }
}
