import * as React from "react";
import { findDOMNode } from "react-dom";
import AutocompleteMenu from "@molecules/AutocompleteMenu";

import { isEventKey } from "keycode";
import fuzzySearch from "@utils/fuzzySearch";

export interface AutocompleteListProps {
    className?: string;
    items: string[];
    itemFormatter?: (value: string, index: number, array: string[]) => string;
    input?: string;
    onRef: (ref: AutocompleteList) => void;
    onSelect?: (item: string) => void;
    isOpen: boolean;
}

export interface AutocompleteListState {
    suggestions: string[];
    activeItemIndex: number;
}

export default class AutocompleteList extends React.PureComponent<AutocompleteListProps, AutocompleteListState> {
    static defaultProps = {
        className: "",
        items: [],
        itemFormatter: input => input,
        input: "",
        onRef: ref => {},
        onSelect: item => {},
        isOpen: false,
    };

    state = {
        suggestions: [],
        activeItemIndex: -1,
    };

    componentDidMount() {
        const { onRef } = this.props;

        document.addEventListener("keydown", this.handleKeyEvents);
        onRef(this);
    }

    componentWillUnmount() {
        const { onRef } = this.props;

        document.removeEventListener("keydown", this.handleKeyEvents);
        onRef(undefined);
    }

    componentDidUpdate(prevProps) {
        const items = this.getFilteredItems(this.props);
        const prevItems = this.getFilteredItems(prevProps);

        // set activeItemIndex to first item if
        // the list items have refreshed
        if (items.length !== prevItems.length) {
            this.setState({
                activeItemIndex: -1,
            });
        }
    }

    handleKeyEvents = (e: Event): void => {
        const { isOpen } = this.props;

        if (!isOpen) return;

        if (isEventKey(e, "down")) {
            this.incrementItemIndex();
        } else if (isEventKey(e, "up")) {
            this.decrementItemIndex();
        }
    }

    incrementItemIndex() {
        const { activeItemIndex } = this.state;
        const listSize = this.getFilteredItems().length;

        const newItemIndex = (activeItemIndex + 1) > listSize - 1
            ? listSize - 1
            : activeItemIndex + 1;

        this.setState({
            activeItemIndex: newItemIndex,
        });
    }

    decrementItemIndex() {
        const { activeItemIndex } = this.state;

        const newItemIndex = (activeItemIndex - 1) < 0
            ? 0
            : activeItemIndex - 1;

        this.setState({
            activeItemIndex: newItemIndex,
        });
    }

    getDOMNode(): HTMLElement {
        return findDOMNode(this) as HTMLElement;
    }

    getFilteredItems(props = this.props): string[] {
        const { input, items, itemFormatter } = props;

        const toSearchItem = (item: string): object => ({ domain: item });
        const toDisplayItem = (item: { domain: string }): string => item.domain;

        const itemsForSearch = items.map(toSearchItem);

        // only find fuzzy match after user has typed
        // at least one character after the "@" symbol
        if (input.substr(0, input.length - 1).includes("@")) {
            const [emailUsername, emailDomain = ""] = input.split("@");

            // used as fallback when fuzzy search doesn't have enough data to work with
            const naiveSearchResult = items.filter(item => item.includes(emailDomain));

            const fuzzySearchResult = fuzzySearch(emailDomain, itemsForSearch, {
                keys: ["domain"],
            }).map(toDisplayItem);

            const results = !fuzzySearchResult.length ? naiveSearchResult : fuzzySearchResult;

            return results.map(itemFormatter);
        } else {
            return items.map(itemFormatter);
        }
    }

    render() {
        const { className, items, onSelect, isOpen } = this.props;
        const { activeItemIndex } = this.state;

        if (!items.length) return null;

        return <AutocompleteMenu
            isOpen={isOpen}
            className={className}
            items={this.getFilteredItems()}
            activeItemIndex={activeItemIndex}
            onSelect={onSelect}
        />;
    }
}
