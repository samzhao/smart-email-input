import * as React from "react";
import Menu, { MenuProps } from "@molecules/Menu";
import "./AutocompleteMenu.scss";

export interface AutocompleteMenuProps extends MenuProps {}

export default class AutocompleteMenu extends React.PureComponent<AutocompleteMenuProps, undefined> {
    static defaultProps = {
        ...Menu.defaultProps,
        isOpen: false,
    };

    render() {
        const { className, items, isOpen, ...restProps } = this.props;

        if (!items.length) return null;
        const autocompleteMenuClasses = `autocomplete-menu ${className} ${isOpen ? "" : "hidden"}`;

        return (
            <div className={autocompleteMenuClasses}>
                <div className="pt-popover pt-minimal pt-select-popover">
                    <div className="pt-popover-content">
                        <Menu isOpen={isOpen} items={items} {...restProps} />
                    </div>
                </div>
            </div>
        );
    }
}
