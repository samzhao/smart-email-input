import * as React from "react";
import "./Menu.scss";
import { isEventKey } from "keycode";
import MenuItem from "@atoms/Highlightable/ListItem";

export interface MenuProps {
    className?: string;
    items?: any[];
    maxHeight?: string | number;
    activeItemIndex?: number;
    onSelect?: (item: string) => void;
    isOpen: boolean;
}

export default class Menu extends React.PureComponent<MenuProps, undefined> {
    static defaultProps = {
        className: "",
        items: [],
        maxHeight: "30vh",
        onSelect: item => {},
        isOpen: true,
    };

    componentDidMount() {
        document.addEventListener("keydown", this.handleKeyEvents);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyEvents);
    }

    handleKeyEvents = (e: Event): void => {
        const { activeItemIndex, onSelect, items, isOpen } = this.props;

        if (!isOpen) return;

        if (isEventKey(e, "enter")) {
            onSelect(items[activeItemIndex]);
        }
    }

    handleSelect = item => () => {
        const { onSelect } = this.props;

        onSelect(item);
    }

    render() {
        const { className, isOpen, items, maxHeight, activeItemIndex } = this.props;

        if (!items.length) return null;

        const menuClasses = `menu pt-menu ${className} ${isOpen ? "" : "hidden"}`;
        const toMenuItem = (item: string, index: number): React.ReactNode =>
            <MenuItem
                key={`menu-item-${index}`}
                isActive={index === activeItemIndex}
                onSelect={this.handleSelect(item)}
            >
                {item}
            </MenuItem>;

        const menuItems = items.map(toMenuItem);

        return (
            <ul className={menuClasses} style={{ maxHeight }}>
                { menuItems }
            </ul>
        );
    }
}
