import * as React from "react";
import { findDOMNode } from "react-dom";
import "./ListItem.scss";

export interface ListItemProps {
    children: React.ReactNode;
    isActive?: boolean;
    onSelect?: () => void;
}

export default class ListItem extends React.PureComponent<ListItemProps, undefined> {
    static defaultProps = {
        isActive: false,
        onSelect: item => {},
    };

    handleClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();

        const { onSelect } = this.props;

        onSelect();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.isActive !== this.props.isActive) {
            this.scrollIntoView();
        }
    }

    scrollIntoView() {
        const itemEl = findDOMNode(this) as HTMLElement;

        if (!itemEl) return;

        itemEl.scrollIntoView({ block: "nearest" });
    }

    render() {
        const { children, isActive } = this.props;

        const anchorClasses = `pt-menu-item ${isActive ? "pt-active" : ""}`;

        return (
            <li>
                <a href="#" className={anchorClasses} onClick={this.handleClick}>
                    { children }
                </a>
            </li>
        );
    }
}
