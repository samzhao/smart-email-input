import * as React from "react";

import { InputGroup, IInputGroupProps } from "@blueprintjs/core";

type InputGroupProps = IInputGroupProps & React.InputHTMLAttributes<HTMLInputElement>;

export interface InputProps extends InputGroupProps {}

export default class Input extends React.PureComponent<InputProps, undefined> {
    render() {
        return <InputGroup large {...this.props} />;
    }
}
