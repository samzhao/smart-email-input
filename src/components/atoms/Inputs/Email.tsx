import * as React from "react";
import ValidatableInput, { ValidatableInputProps } from "./ValidatableInput";

export interface EmailProps extends ValidatableInputProps {}

export default class Email extends React.PureComponent<EmailProps, undefined> {
    render() {
        return <ValidatableInput
            leftIcon="envelope"
            placeholder="Enter your email here"
            autoComplete="off"
            {...this.props}
        />;
    }
}
