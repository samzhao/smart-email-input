import * as React from "react";
import { Icon, Intent, Button, Spinner } from "@blueprintjs/core";
import { IconName, IconNames } from "@blueprintjs/icons";
import Input, { InputProps } from "./Input";

export enum ValidationStatus {
    IDLE,
    IN_PROGRESS,
    SUCCEEDED = Intent.SUCCESS,
    FAILED = Intent.DANGER,
}

export interface ValidatableInputProps extends InputProps {
    validationStatus?: ValidationStatus;
    error?: any;
}

export default class ValidatableInput extends React.PureComponent<ValidatableInputProps, undefined> {
    static defaultProps = {
        validationStatus: ValidationStatus.IDLE,
    };

    renderIcon(icon: IconName, intent: Intent): JSX.Element {
        return (
            <div className="input-icon-wrapper">
                <Icon icon={icon} intent={intent} />
            </div>
        );
    }

    getInputIntent(): Intent {
        const { validationStatus } = this.props;

        switch (validationStatus) {
            case ValidationStatus.SUCCEEDED:
                return Intent.SUCCESS;
            case ValidationStatus.FAILED:
                return Intent.DANGER;
            case ValidationStatus.IDLE:
            case ValidationStatus.IN_PROGRESS:
            default:
                return Intent.NONE;
        }
    }

    getRightElement(): JSX.Element {
        const { validationStatus } = this.props;

        switch (validationStatus) {
            case ValidationStatus.IN_PROGRESS:
                return <Spinner small />;
            case ValidationStatus.SUCCEEDED:
                return this.renderIcon(IconNames.TICK_CIRCLE, Intent.SUCCESS);
            case ValidationStatus.FAILED:
                return this.renderIcon(IconNames.ISSUE, Intent.DANGER);
            case ValidationStatus.IDLE:
            default:
                return null;
        }
    }

    render() {
        const { validationStatus, ...restProps } = this.props;

        return <Input
            intent={this.getInputIntent()}
            rightElement={this.getRightElement()}
            {...restProps}
        />;
    }
}
