import * as React from "react";
import isEmailValid from "@utils/validateEmail";
import getEmailSuggestion from "@utils/getEmailSuggestion";

export interface EmailSuggestionProps {
    email: string;
    onSuggestion?: (suggestion: string) => void;
    onSuggestionAccepted?: (suggestion: string) => void;
    defaultText?: React.ReactNode;
}

export interface EmailSuggestionState {
    suggestion: string;
}

export default class EmailSuggestion extends React.PureComponent<EmailSuggestionProps, EmailSuggestionState> {
    static defaultProps = {
        onSuggestion: suggestion => {},
        onSuggestionAccepted: suggestion => {},
        defaultText: null,
    };

    state = {
        suggestion: "",
    };

    componentDidUpdate(prevProps, prevState) {
        const { email } = this.props;
        const { email: prevEmail } = prevProps;
        const { suggestion: prevSuggestion } = prevState;

        if (email === prevSuggestion) {
            this.resetSuggestion();
            return;
        }

        if (email !== prevEmail) {
            if (isEmailValid(email)) {
                this.updateSuggestion();
            } else {
                this.resetSuggestion();
            }
        }
    }

    updateSuggestion() {
        const { email, onSuggestion } = this.props;

        getEmailSuggestion(email).then(suggestion => {
            onSuggestion(suggestion);
            this.setState({ suggestion });
        }).catch(err => {});
    }

    resetSuggestion() {
        this.setState({ suggestion: "" });
    }

    acceptSuggestion = (e: React.MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();

        const { onSuggestionAccepted } = this.props;
        const { suggestion } = this.state;

        onSuggestionAccepted(suggestion);
    }

    render() {
        const { email, defaultText } = this.props;
        const { suggestion } = this.state;

        // don't render anything when either the email
        // is absent or there is no suggested email
        if (!email || !suggestion) return defaultText;

        return (
            <span className="email-suggestion">
                Did you mean <a href="" onClick={this.acceptSuggestion}>{suggestion}</a>?
            </span>
        );
    }
}
