import * as React from "react";
import "./EmailInput.scss";

import { FormGroup } from "@blueprintjs/core";

import { isEventKey } from "keycode";
import * as _ from "lodash";
import { EMAIL_DOMAINS } from "@constants/index";
import validateEmail from "@utils/validateEmail";
import { ContainerProps } from "@containers/EmailVerification";
import { EmailVerificationAction, EmailVerificationState } from "@store/emailVerification/types";

import Email from "@atoms/Inputs/Email";
import EmailSuggestion from "@atoms/EmailSuggestion";
import AutocompleteList from "@molecules/AutocompleteList";
import { ValidationStatus } from "@atoms/Inputs/ValidatableInput";

export interface EmailInputProps extends ContainerProps {
}

export interface EmailInputState {
    email: string;
    isAutocompleteListOpen: boolean;
}

const LIST_STATUS = {
    OPEN: true,
    CLOSE: false,
};

export default class EmailInput extends React.PureComponent<EmailInputProps, EmailInputState> {
    autocompleteList: AutocompleteList = null;
    emailInput: HTMLInputElement = null;

    state = {
        email: "",
        isAutocompleteListOpen: false,
    };

    setAsEmail = (newEmail: string, cb?: () => void) => {
        this.setState({
            email: newEmail,
        }, cb);
    }

    setAutocompletelistRef = ref => {
        this.autocompleteList = ref;
    }

    setAutocompleteListStatus = (status: boolean) => {
        this.setState({
            isAutocompleteListOpen: status,
        });
    }

    verifyEmail = () => {
        const { startVerification } = this.props;
        const { email } = this.state;

        email && startVerification(email);
    }

    throttledVerifyEmail = _.debounce(this.verifyEmail, 1000, {
        leading: false,
        trailing: true,
    });

    resetVerification = () => {
        const { resetVerification } = this.props;

        resetVerification();
    }

    handleKeyEvents = (e: React.KeyboardEvent<HTMLInputElement> & Event) => {
        const { email, isAutocompleteListOpen } = this.state;
        const isKey = (key: string): boolean => isEventKey(e, key);

        if (isKey("space")) {
            e.preventDefault();
        }

        if (!this.autocompleteList) return;

        if (isKey("down") && !isAutocompleteListOpen && email) {
            e.preventDefault();
            this.setAutocompleteListStatus(LIST_STATUS.OPEN);
        } else if (isKey("up") && isAutocompleteListOpen) {
            e.preventDefault();
        } else {}
    }

    handleEmailChange = (e: React.FormEvent<HTMLInputElement>) => {
        const value = e.currentTarget.value;
        const hasValue = Boolean(value);
        const isAutocompleteListOpen = hasValue;

        this.resetVerification();
        this.setAsEmail(value, () => {
            this.throttledVerifyEmail();
        });
        this.setAutocompleteListStatus(isAutocompleteListOpen);
    }

    handleEmailFocus = (e: React.FocusEvent<HTMLInputElement>) => {
        const { email } = this.state;

        if (!email) return;

        this.setAutocompleteListStatus(LIST_STATUS.OPEN);
    }

    handleEmailBlur = (e: React.FocusEvent<HTMLInputElement>) => {
        const blurredToNode = e.relatedTarget as HTMLElement;

        const autocompleteListNode = this.autocompleteList.getDOMNode();
        if (autocompleteListNode && autocompleteListNode.contains(blurredToNode)) {
            if (this.emailInput) this.emailInput.focus();
        } else {
            this.setAutocompleteListStatus(LIST_STATUS.CLOSE);
        }

        this.verifyEmail();
    }

    getAutocompleteCandidates(): string[] {
        const { email } = this.state;

        if (email.startsWith("@")) return [];

        return EMAIL_DOMAINS;
    }

    autocompleteItemFormatter = (domain: string): string => {
        const { email } = this.state;

        const [emailUsername] = email.split("@");

        return `${emailUsername}@${domain}`;
    }

    onAutocompleteItemSelected = (item: string) => {
        if (!item) {
            this.setAutocompleteListStatus(LIST_STATUS.CLOSE);
        } else {
            this.setAsEmail(item, () => {
                this.verifyEmail();
            });
        }

        this.setAutocompleteListStatus(LIST_STATUS.CLOSE);
    }

    onEmailSuggestionAccepted = (suggestedEmail: string) => {
        this.setAsEmail(suggestedEmail, () => {
            this.verifyEmail();
        });
    }

    getInputValidationStatus(): ValidationStatus {
        const { isVerified, isVerifying, hasError, error } = this.props;

        if (isVerifying) {
            return ValidationStatus.IN_PROGRESS;
        } else if (isVerified) {
            return ValidationStatus.SUCCEEDED;
        } else if (hasError) {
            return ValidationStatus.FAILED;
        } else {
            return ValidationStatus.IDLE;
        }
    }

    getHelperText(): React.ReactNode {
        const { error, hasError, isVerifying, isVerified, message = "" } = this.props;
        const defaultHelperText = "Helpful hints will appear as you type :)";

        if (hasError) {
            return error.message;
        } else if (isVerifying) {
            return message || <br/>;
        } else if (isVerified) {
            return "Nice email address! You may proceed :)";
        } else {
            return defaultHelperText;
        }
    }

    render() {
        const { email, isAutocompleteListOpen } = this.state;

        return (
            <div className="email-input">
                <FormGroup
                    label="Email"
                    labelFor="email-input"
                    helperText={
                        <EmailSuggestion
                            email={email}
                            defaultText={this.getHelperText()}
                            onSuggestionAccepted={this.onEmailSuggestionAccepted}
                        />
                    }
                >
                    <Email
                        id="email-input"
                        inputRef={ref => this.emailInput = ref}
                        value={email}
                        onKeyDown={this.handleKeyEvents}
                        onChange={this.handleEmailChange}
                        onFocus={this.handleEmailFocus}
                        onBlur={this.handleEmailBlur}
                        validationStatus={this.getInputValidationStatus()}
                        autoFocus
                    />

                    <AutocompleteList
                        input={email}
                        items={this.getAutocompleteCandidates()}
                        isOpen={isAutocompleteListOpen}
                        itemFormatter={this.autocompleteItemFormatter}
                        onRef={this.setAutocompletelistRef}
                        onSelect={this.onAutocompleteItemSelected}
                    />
                </FormGroup>
            </div>
        );
    }
}
