export interface EmailVerificationAPIResponse {
    data: {
        result: string;
        success: boolean;
    };
}

// for now we will treat risky results as valid
const validResults = [
    "deliverable",
    "risky",
];

const invalidResults = [
    "undeliverable",
];

const errorResults = [
    "unknown",
];

export interface EmailVerificationError {
    message: string;
}

export class EmailVerificationResult {
    private result;

    constructor(response: EmailVerificationAPIResponse) {
        this.result = response.data.result;
    }

    public isValid() {
        return validResults.indexOf(this.result) > -1;
    }

    public isInvalid() {
        return invalidResults.indexOf(this.result) > -1;
    }

    // if result is in known error results and NOT in known valid or invalid results
    public hasError() {
        return errorResults.indexOf(this.result) > -1
            && validResults.concat(invalidResults).indexOf(this.result) < 0;
    }
}
