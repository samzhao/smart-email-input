import axios from "axios";

const apiURL = process.env.API_URL || "";
const emailVerificationURL = `${apiURL}/verifyEmail`;

class EmailVerificationAPI {
    verifyEmail(email) {
        return axios.post(emailVerificationURL, { email });
    }
}

export default new EmailVerificationAPI();
