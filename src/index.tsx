import * as React from "react";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";

import { Provider } from "react-redux";
import configureStore from "@utils/configureStore";

import "./index.scss";
import App from "@pages/App";

const rootEl = document.getElementById("root");

function renderApp(Entry) {
    const store = configureStore();

    render(
        <Provider store={store}>
            <AppContainer>
                <Entry />
            </AppContainer>
        </Provider>,
        rootEl
    );
}

renderApp(App);

// Hot Module Replacement API
declare let module: { hot: any };

if (module.hot) {
    module.hot.accept("./components/pages/App", () => {
        const NewApp = require("./components/pages/App").default;

        renderApp(NewApp);
    });
}
