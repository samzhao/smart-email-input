# Intuitive Email Input

This project is built on top of the awesome [React Webpack Typescript Starter](https://github.com/vikpe/react-webpack-typescript-starter)

## Prerequisites

1. node version `^8.6.0`;
2. npm version `5.x`

## Running the app

To run the app, first make sure all the npm packages are installed using `npm install`.
After that, simply run: `npm start` or `yarn start`, and the app should open in your default browser.

## State management

The app state is managed using Redux with the help of `react-redux`. Async actions are handled using `redux-saga`.

Async actions are used when verifying emails with the third-party [Kickbox API](https://kickbox.com/).

The API key can be found (and replaced) in the `.env` file located in the project root.

## Atomic design

The components are structured using [atomic design](http://bradfrost.com/blog/post/atomic-web-design/) methodology.

- Atoms are the smallest building blocks. E.g.: a Dropdown list item or an input element.
- Molecules group atoms together. E.g.: a Dropdown list that contains multiple list items.
- Organisms group molecules or atoms together. E.g.: an autocomplete list that uses a dropdown and an input.
- Pages are constructed by grouping organisms together. E.g.: an email form that uses autocomplete.

## Features

1. Autocomplete suggestions of a select list of email domains as the user types;
2. "Did you mean" catches common misspellings, such as typing ".con" instead of ".com" or "gmil" instead of "gmail";
3. Keyboard interactivity in autocomplete suggestions dropdown;
4. Email deliverability check using Kickbox API and front-end Regex check of email format;
5. Displaying of verification API request status (i.e. "Checking email address...") if request takes longer 500 milliseconds to complete.
