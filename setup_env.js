const fs = require('fs');
const { COPYFILE_EXCL } = fs.constants;

try {
  fs.copyFileSync('.env.example', '.env', COPYFILE_EXCL); 
  console.log('.env file created.');
} catch (_) {}
