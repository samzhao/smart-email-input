const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const app = express();
const request = require('request');

require('dotenv').config();

const hostname = process.env.SERVER_HOST || 'localhost';
const portNumber = process.env.SERVER_PORT || 3000;
const sourceDir = path.resolve(__dirname, './dist');

app.use(express.static(sourceDir));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const apiRoutes = express.Router();

const kickboxApiKey = process.env.KICKBOX_API_KEY || "";
const emailVerificationURL = `https://api.kickbox.com/v2/verify?apikey=${kickboxApiKey}`;

apiRoutes.post('/verifyEmail', (req, res) => {
    const { email = "" } = req.body;
    const reqURL = `${emailVerificationURL}&email=${email}`;

    request
        .get(reqURL)
        .pipe(res);
});

app.get('*', (req, res) => {
    res.sendFile(`${sourceDir}/index.html`);
});
app.use('/api', apiRoutes);

app.listen(portNumber, hostname, () => {
  console.log(`Express web server started: http://${hostname}:${portNumber}`);
  console.log(`Serving content from /${sourceDir}/`);
});
